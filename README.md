Node v14.16.1
<br>
TypeScript v4.1.3
<br>
ts-node v9.1.1
<br>

1 npm i
<br>
2 npm run build
<br>
3 sh docker/build.sh
<br>
4 cd docker/
<br>
5 docker-compose up -d
<br>
6 sh docker/seed.sh
<br>
7 npm run emulator
<br>

And so what to do?
<br>
See all request api localhost:port/api
<br>
1 /user/without-invite - get one of id from item
<br>
2 /presentation/generateInvite - generate invite :)
<br>
3 /presentation/checkTicketByInviteCode - pass an invite code)

