import { configService } from './src/config.service';

export default {
  ...configService.getTypeOrmConfig(),
};
