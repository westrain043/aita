import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';

require('dotenv').config();

type TKey =
  | 'POSTGRES_HOST'
  | 'POSTGRES_PORT'
  | 'POSTGRES_USER'
  | 'POSTGRES_PASSWORD'
  | 'POSTGRES_DATABASE'
  | 'REDIS_HOST'
  | 'PORT';

export class ConfigService {
  constructor(private env: { [k: string]: string | undefined }) {}

  private getValue(key: TKey, throwOnMissing = true) {
    const value = this.env[key];
    if (!value && throwOnMissing) {
      throw new Error(`config error - missing env.${key}`);
    }

    return value;
  }

  public ensureValues(keys: TKey[]) {
    keys.forEach((k) => this.getValue(k, true));
    return this;
  }

  public getRedis() {
    return this.getValue('REDIS_HOST');
  }

  public getPort() {
    return this.getValue('PORT');
  }

  public getTypeOrmConfig(): TypeOrmModuleOptions {
    return {
      type: 'postgres',

      host: this.getValue('POSTGRES_HOST'),
      port: parseInt(this.getValue('POSTGRES_PORT')!),
      username: this.getValue('POSTGRES_USER'),
      password: this.getValue('POSTGRES_PASSWORD'),
      database: this.getValue('POSTGRES_DATABASE'),

      entities: ['dist/src/entities/*.entity.js'],

      migrationsTableName: 'migrations',

      migrations: ['dist/src/migrations/*.js'],

      subscribers: ['dist/src/subscribers/*.js'],

      seeds: ['dist/src/seeds/*.js'],

      factories: ['dist/src/factories/*.js'],

      cli: {
        migrationsDir: 'src/migrations',
      },

      synchronize: true,

      namingStrategy: new SnakeNamingStrategy(),
      logging: true,
    } as TypeOrmModuleOptions;
  }
}

const configService = new ConfigService(process.env).ensureValues([
  'POSTGRES_HOST',
  'POSTGRES_PORT',
  'POSTGRES_USER',
  'POSTGRES_PASSWORD',
  'POSTGRES_DATABASE',
  'REDIS_HOST',
  'PORT',
]);

export { configService };
