export enum ETicketStatus {
  DEACTIVE = 'DEACTIVE',
  ACTIVE = 'ACTIVE',
  IN_MEETUP = 'IN_MEETUP',
}
