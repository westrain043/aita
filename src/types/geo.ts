import { ApiProperty } from '@nestjs/swagger';
import { ValueTransformer } from 'typeorm';

export class Coordinates {
  @ApiProperty()
  latitude!: number;
  @ApiProperty()
  longitude!: number;
}

export class PointTransformer implements ValueTransformer {
  to(value: Coordinates) {
    const { latitude, longitude } = value;
    return `${latitude}, ${longitude}`;
  }

  from(value: { x: number; y: number }): Coordinates {
    const { x: latitude, y: longitude } = value;
    return { latitude, longitude };
  }
}
