import { User } from '@entities/user.entity';
import { Country } from '@entities/country.entity';
import { Flight } from '@entities/flight.entity';
import { Connection } from 'typeorm';
import { Factory, Seeder } from 'typeorm-seeding';
import _, { random, sample, uniqBy } from 'lodash';
import { Airport } from '@entities/airport.entity';
import { data, generateCode, getDistanceInKm } from '../helpers';
import { City } from '@entities/city.entity';
import { Ticket } from '@entities/ticket.entity';
import { ETicketStatus } from '../types/enums';

export default class Seeding implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<void> {
    await connection.transaction(async (manager) => {
      const usersRepo = manager.getRepository(User);
      const countriesRepo = manager.getRepository(Country);
      const cityRepo = manager.getRepository(City);
      const airportsRepo = manager.getRepository(Airport);

      const users = await factory(User)().makeMany(10);
      const tickets = await factory(Ticket)().makeMany(5);

      const uniqCountries = uniqBy(data, 'country');

      const countries = await countriesRepo.save(
        uniqCountries.map((e) => ({ name: e.country })),
      );

      const cities = cityRepo.create(
        data.map((e) => ({
          name: e.city,
          country: countries.find((f) => f.name === e.country),
        })),
      );

      const airports = airportsRepo.create(
        data.map((e) => {
          const [latitude, longitude] = e.coordinates
            .split(',')
            .map((e) => parseFloat(e));

          return {
            name: e.airport,
            city: cities.find((f) => f.name === e.city),
            coordinates: { latitude, longitude },
          };
        }),
      );

      const getDestination = (departure: Airport): Airport => {
        const arrival = sample(airports);
        if (departure === arrival) return getDestination(departure);
        return arrival!;
      };

      const getDestinationInfo = (
        departure: Airport,
        arrival: Airport,
        departureDate: Date,
      ) => {
        const speed = 700;

        const distance = getDistanceInKm(
          departure.coordinates.latitude,
          departure.coordinates.longitude,
          arrival.coordinates.latitude,
          arrival.coordinates.longitude,
        );

        const time = (distance / speed) * 3600 * 1000;

        return {
          arrivalDate: departureDate.getTime() + time,
          distance,
        };
      };

      const flightFunc = (user: User) => {
        const flight = new Flight();

        const departure = sample(airports);

        const destination = getDestination(departure!);

        flight.user = user;
        flight.departure = departure!;
        flight.destination = destination;

        flight.departureDate = new Date();

        const { arrivalDate, distance } = getDestinationInfo(
          departure!,
          destination,
          flight.departureDate,
        );

        flight.arrivalDate = new Date(arrivalDate);

        flight.distance = distance;

        return flight;
      };

      const result: Flight[] = [];

      const userForTickets = [...users];

      tickets.map((ticket) => {
        ticket.user = userForTickets.shift()!;
        ticket.inviteCode = generateCode(tickets, random(40000, 99999));
        ticket.status = [ETicketStatus.ACTIVE, ETicketStatus.IN_MEETUP][
          random(0, 1)
        ];
        return ticket;
      });

      for (let i = 0; i < 20; i++)
        result.push(flightFunc(users[random(0, users.length - 1)]));

      await usersRepo.save(users);

      await manager.getRepository(Ticket).save(tickets);

      await manager.getRepository(Flight).save(result);
    });
  }
}
