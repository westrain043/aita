import Faker from 'faker';
import { User } from '@entities/user.entity';
import { define, factory } from 'typeorm-seeding';
import { Ticket } from '@entities/ticket.entity';

define(Ticket, (faker: typeof Faker) => {
  const ticket = new Ticket();
  ticket.inviteCode = faker.random.number({ min: 4000, max: 9999 });
  ticket.user = factory(User)() as any;
  return ticket;
});
