import Faker from 'faker';
import { User } from '@entities/user.entity';
import { define } from 'typeorm-seeding';

define(User, (faker: typeof Faker) => {
  const user = new User();
  user.firstName = faker.name.firstName();
  user.lastName = faker.name.firstName();
  user.login = faker.random.word();
  user.password = faker.random.word();
  user.birthday = faker.date.between('01.01.1990', '01.01.2021');
  return user;
});
