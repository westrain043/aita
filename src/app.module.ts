import { Module, OnModuleInit } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from 'services/users.service/user.module';
import { configService } from './config.service';
import { Airport } from './entities/airport.entity';
import { City } from './entities/city.entity';
import { Country } from './entities/country.entity';
import { Flight } from './entities/flight.entity';
import { Ticket } from './entities/ticket.entity';
import { User } from './entities/user.entity';
import { PresentationModule } from './services/presentation.service/presentation.module';
import { RedisService } from './services/redis.service';
import { TicketSubscriber } from './subscribers/ticket.subscriber';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      ...configService.getTypeOrmConfig(),
      entities: [Airport, City, Country, Flight, Ticket, User],
      subscribers: [TicketSubscriber],
    }),
    PresentationModule,
    UserModule,
  ],
  providers: [],
})
export class AppModule implements OnModuleInit {
  async onModuleInit() {
    (await RedisService.getInstance()).subscribe('presentation', (err) => {
      if (err) console.error('Failed to subscribe: %s', err.message);
      else console.log('Subscribed successfully!');
    });
  }
}
