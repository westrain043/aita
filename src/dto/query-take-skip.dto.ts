import { ApiPropertyOptional } from '@nestjs/swagger';
import { Transform, TransformFnParams } from 'class-transformer';
import { IsNumber, IsOptional } from 'class-validator';

export class QueryTakeSkipDto {
  @ApiPropertyOptional({ default: 5 })
  @IsOptional()
  @IsNumber()
  @Transform((params: TransformFnParams) => parseInt(params.value))
  take?: number;
  @ApiPropertyOptional({ default: 0 })
  @IsOptional()
  @IsNumber()
  @Transform((params: TransformFnParams) => parseInt(params.value))
  skip?: number;
}
