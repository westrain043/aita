import { ApiProperty } from '@nestjs/swagger';
import { createHmac } from 'crypto';
import {
  BeforeInsert,
  Column,
  CreateDateColumn,
  Entity,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Ticket } from './ticket.entity';

@Entity({ name: 'users' })
export class User {
  @ApiProperty()
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @ApiProperty()
  @Column()
  firstName!: string;

  @ApiProperty()
  @Column()
  lastName!: string;

  @ApiProperty()
  @Column({ type: 'date' })
  birthday!: Date;

  @ApiProperty()
  @Column()
  login!: string;

  @Column({ select: false })
  password!: string;

  @BeforeInsert()
  hashPassword() {
    if (this.password) {
      this.password = createHmac('sha256', this.password).digest('hex');
    }
  }

  @ApiProperty({ type: () => Ticket })
  @OneToOne(() => Ticket, (ticket) => ticket.user)
  actualTicket!: Ticket;

  @CreateDateColumn()
  createdAt!: Date;
}
