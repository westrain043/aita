import { ApiProperty } from '@nestjs/swagger';
import { Coordinates, PointTransformer } from '../types/geo';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { City } from './city.entity';

@Entity({ name: 'airport' })
export class Airport {
  @ApiProperty()
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @ApiProperty()
  @Column({ unique: true })
  name!: string;

  @ApiProperty()
  @Column({
    type: 'point',
    transformer: new PointTransformer(),
  })
  coordinates!: Coordinates;

  @ApiProperty()
  @ManyToOne(() => City, (city) => city.id, {
    cascade: true,
  })
  @JoinColumn()
  city!: City;
}
