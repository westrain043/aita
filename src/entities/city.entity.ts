import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Country } from './country.entity';

@Entity({ name: 'city' })
export class City {
  @ApiProperty()
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @ApiProperty()
  @Column({ unique: true })
  name!: string;

  @ApiProperty()
  @ManyToOne(() => Country, (country) => country.id, {
    cascade: true,
  })
  @JoinColumn()
  country!: Country;
}
