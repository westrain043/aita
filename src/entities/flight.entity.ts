import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { User } from '@entities/user.entity';
import { Airport } from './airport.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity({ name: 'flights' })
export class Flight {
  @ApiProperty()
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @ApiProperty({ type: () => User })
  @ManyToOne(() => User, (user) => user.id, {
    cascade: true,
  })
  @JoinColumn()
  user!: User;

  @ApiProperty()
  @ManyToOne(() => Airport, (airport) => airport.id, {
    cascade: true,
  })
  @JoinColumn()
  departure!: Airport;

  @ApiProperty()
  @ManyToOne(() => Airport, (airport) => airport.id, {
    cascade: true,
  })
  @JoinColumn()
  destination!: Airport;

  @ApiProperty()
  @Column('timestamptz')
  departureDate!: Date;

  @ApiProperty()
  @Column('timestamptz')
  arrivalDate!: Date;

  @ApiProperty()
  @Column()
  distance!: number;

  @ApiProperty()
  @UpdateDateColumn()
  updateAt!: Date;
}
