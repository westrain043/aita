import { ApiProperty } from '@nestjs/swagger';
import { ETicketStatus } from '../types/enums';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { User } from './user.entity';

@Entity({ name: 'tickets' })
export class Ticket {
  @ApiProperty()
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @ApiProperty()
  @Column({ unique: true })
  inviteCode!: number;

  @ApiProperty()
  @Column({ type: 'enum', enum: ETicketStatus, default: ETicketStatus.ACTIVE })
  status!: ETicketStatus;

  @ApiProperty({ type: () => User })
  @ManyToOne(() => User, (user) => user.id, {
    cascade: true,
  })
  @JoinColumn()
  user!: User;

  @Column({ unique: false })
  user_id!: string;

  @ApiProperty()
  @UpdateDateColumn()
  updateAt!: Date;
}
