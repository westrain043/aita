import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { join } from 'path';
import { NestExpressApplication } from '@nestjs/platform-express';
import { PresentationModule } from './services/presentation.service/presentation.module';
import { configService } from './config.service';
import { UserModule } from 'services/users.service/user.module';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.useStaticAssets(join(__dirname, '..', 'static'));
  app.setGlobalPrefix('api');

  const config = new DocumentBuilder()
    .setTitle('Aita')
    .setDescription('The Aita API')
    .setVersion('1.0')
    .build();
  const appDocument = SwaggerModule.createDocument(app, config, {
    include: [PresentationModule, UserModule],
  });
  SwaggerModule.setup('api', app, appDocument);

  const port = configService.getPort();

  await app.listen(port!, () => {
    console.log(`The service is running on http://localhost:${port}`);
    console.log(`Swagger http://localhost:${port}/api`);
  });
}
bootstrap();
