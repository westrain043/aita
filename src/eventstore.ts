import { Injectable } from '@nestjs/common';

export class EventStore {
  private constructor() {}
  private static instance: EventStore;
  private subscribers: Map<string, (data: any) => Promise<void>> = new Map();

  public static getInstance() {
    if (!EventStore.instance) EventStore.instance = new EventStore();
    return EventStore.instance;
  }

  private getKey(event: 'MAP' | 'BOARDING', subscriber?: string): string {
    let key = event;
    if (subscriber) key += `_${subscriber}`;
    return key;
  }

  on(
    event: 'MAP' | 'BOARDING',
    callback: (data: any) => any,
    subscriber?: string,
  ) {
    this.subscribers.set(this.getKey(event, subscriber), callback);
  }

  publish(event: 'MAP' | 'BOARDING', data: any, subscriber?: string) {
    const foundSubscriber = this.subscribers.get(
      this.getKey(event, subscriber),
    );

    if (foundSubscriber) return foundSubscriber(data);
  }
}
