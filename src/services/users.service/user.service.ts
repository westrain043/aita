import { Ticket } from '@entities/ticket.entity';
import { User } from '@entities/user.entity';
import { Injectable } from '@nestjs/common';
import { QueryTakeSkipDto } from 'dto/query-take-skip.dto';
import { Connection } from 'typeorm';
import { ETicketStatus } from 'types/enums';
import { UserIdDto } from './dto/user-id.dto';

@Injectable()
export class UserService {
  constructor(private connection: Connection) {}

  async getUser({ userId }: UserIdDto) {
    return this.connection.manager.getRepository(User).findOne({
      where: {
        id: userId,
      },
    });
  }

  async getUsers({ take = 5, skip = 0 }: QueryTakeSkipDto) {
    return this.connection.manager.getRepository(User).find({
      take,
      skip,
    });
  }

  async getUsersWithoutInvite({ take = 5, skip = 0 }: QueryTakeSkipDto) {
    return this.connection.manager
      .getRepository(User)
      .createQueryBuilder('u')
      .select('u')
      .leftJoin(Ticket, 't', 't.user_id = u.id')
      .groupBy('u.id')
      .where('t.status = :status', {
        status: ETicketStatus.DEACTIVE,
      })
      .orWhere('t is null')
      .take(take)
      .skip(skip)
      .getMany();
  }
}
