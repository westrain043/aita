import { User } from '@entities/user.entity';
import { Controller, Get, Param, Query, ValidationPipe } from '@nestjs/common';
import { ApiCreatedResponse } from '@nestjs/swagger';
import { QueryTakeSkipDto } from 'dto/query-take-skip.dto';
import { UserIdDto } from './dto/user-id.dto';
import { UserService } from './user.service';

@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @ApiCreatedResponse({ type: User })
  @Get('/user/:userId')
  async getUser(@Param(new ValidationPipe()) param: UserIdDto) {
    return this.userService.getUser({
      userId: param.userId,
    });
  }

  @ApiCreatedResponse({ type: User })
  @Get('/list')
  async getUsers(@Query(new ValidationPipe()) query: QueryTakeSkipDto) {
    return this.userService.getUsers(query);
  }

  @ApiCreatedResponse({ type: User })
  @Get('/without-invite')
  async getUsersWithoutInvite(
    @Query(new ValidationPipe()) query: QueryTakeSkipDto,
  ) {
    return await this.userService.getUsersWithoutInvite(query);
  }
}
