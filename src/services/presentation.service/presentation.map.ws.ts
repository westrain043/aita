import {
  OnGatewayConnection,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { QueryTakeSkipDto } from 'dto/query-take-skip.dto';
import { UserIdDto } from 'services/users.service/dto/user-id.dto';
import { Server, Socket } from 'socket.io';
import { RedisService } from '../redis.service';
import { PresentationService } from './presentation.service';

@WebSocketGateway({ namespace: '/map' })
export class PresentationMapWS implements OnGatewayConnection {
  @WebSocketServer() server!: Server;

  constructor(private readonly presentation: PresentationService) {}

  async handleConnection(client: Socket) {
    (await RedisService.getInstance()).on('message', async (message) => {
      const jmessage: UserIdDto & QueryTakeSkipDto = JSON.parse(message);
      const res = await this.presentation.map(jmessage);
      client.emit('mapClient', res);
    });
  }
}
