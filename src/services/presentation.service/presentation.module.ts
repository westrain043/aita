import { Module } from '@nestjs/common';
import { PresentationController } from './presentation.controller';
import { PresentationService } from './presentation.service';
import { PresentationMapWS } from './presentation.map.ws';
import { PresentationBoardingWS } from './presentation.boarding.ws';

@Module({
  controllers: [PresentationController],
  providers: [PresentationService, PresentationMapWS, PresentationBoardingWS],
})
export class PresentationModule {}
