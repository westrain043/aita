import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';
import { Flight } from '@entities/flight.entity';
import { Ticket } from '@entities/ticket.entity';
import { User } from '@entities/user.entity';
import { ETicketStatus } from '../../types/enums';
import { random } from 'lodash';
import { Connection, EntityManager, In } from 'typeorm';
import { QueryTakeSkipDto } from 'dto/query-take-skip.dto';
import { UserIdDto } from 'services/users.service/dto/user-id.dto';
export class TCheckInviteResult {
  @ApiProperty()
  ticketId!: string;
  @ApiProperty()
  firstName!: string;
}

@Injectable()
export class PresentationService {
  constructor(private connection: Connection) {}

  async generateInvite(userId: string) {
    return await this.connection.transaction(async (manager: EntityManager) => {
      const user = await manager.getRepository(User).findOne(userId);

      if (!user)
        throw new HttpException(
          `User [${userId}] not found!`,
          HttpStatus.BAD_REQUEST,
        );

      const ticket = await manager.getRepository(Ticket).findOne({
        where: {
          user: { id: userId },
          status: In([ETicketStatus.ACTIVE, ETicketStatus.IN_MEETUP]),
        },
      });

      if (ticket)
        throw new HttpException(
          `The user already has a ticket [${ticket.inviteCode}]`,
          HttpStatus.BAD_REQUEST,
        );

      const genInviteCode = async (code: number): Promise<number> => {
        const found = await manager
          .getRepository(Ticket)
          .findOne({ where: { inviteCode: code } });

        if (!found) {
          return code;
        }

        return await genInviteCode(random(40000, 99999));
      };

      const inviteCode = random(40000, 99999);

      return manager
        .getRepository(Ticket)
        .save({ inviteCode: await genInviteCode(inviteCode), user });
    });
  }

  async getTicketByCode(
    inviteCode: number,
    status: ETicketStatus = ETicketStatus.ACTIVE,
  ) {
    const result = await this.connection
      .getRepository(Ticket)
      .findOne({ where: { inviteCode, status } });
    return result;
  }

  async getTicketById(
    id: string,
    status: ETicketStatus = ETicketStatus.ACTIVE,
  ) {
    const result = await this.connection
      .getRepository(Ticket)
      .findOne({ where: { id, status } });
    return result;
  }

  async checkInviteByCode(inviteCode: number): Promise<TCheckInviteResult> {
    const result = await this.getTicketByCode(inviteCode);
    if (!result) throw new HttpException('Not valid', HttpStatus.BAD_REQUEST);

    const user = await this.connection.manager
      .getRepository(User)
      .findOne(result.user_id);

    const { firstName } = user!;

    return { ticketId: result.id, firstName };
  }

  async changeStatusTicketToInMeetup(
    id: string,
    status: ETicketStatus = ETicketStatus.IN_MEETUP,
  ) {
    const result = await this.getTicketById(id);

    if (!result) throw new HttpException('Not found', HttpStatus.BAD_REQUEST);

    return await this.connection.getRepository(Ticket).save({ id, status });
  }

  async boarding() {
    return this.connection.manager
      .createQueryBuilder(Flight, 'flight')
      .innerJoinAndSelect('flight.user', 'user')
      .innerJoinAndSelect('user.actualTicket', 'actualTicket')
      .innerJoinAndSelect('flight.destination', 'destination')
      .where('actualTicket.status = :status', {
        status: ETicketStatus.IN_MEETUP,
      })
      .orderBy('flight.update_at', 'DESC')
      .addOrderBy('actualTicket.update_at', 'DESC')
      .getOne();
  }

  async map({ userId, take = 5, skip = 0 }: UserIdDto & QueryTakeSkipDto) {
    return this.connection.manager.getRepository(Flight).find({
      where: {
        user: { id: userId },
      },
      take,
      skip,
      relations: ['user', 'destination', 'departure'],
    });
  }
}
