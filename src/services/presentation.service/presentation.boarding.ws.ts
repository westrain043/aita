import {
  OnGatewayConnection,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { EventStore } from '../../eventstore';
import { Server, Socket } from 'socket.io';
import { PresentationService } from './presentation.service';

@WebSocketGateway({ namespace: '/boarding' })
export class PresentationBoardingWS implements OnGatewayConnection {
  @WebSocketServer() server!: Server;

  constructor(private readonly presentation: PresentationService) {}

  async handleConnection(client: Socket) {
    const eventSource = EventStore.getInstance();
    eventSource.on('BOARDING', async () => {
      const res = await this.presentation.boarding();
      client.emit('boardingClient', res);
    });
  }
}
