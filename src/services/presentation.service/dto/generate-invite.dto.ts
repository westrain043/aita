import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';

export class GenerateInviteDto {
  @ApiProperty()
  @IsUUID('4')
  @IsNotEmpty()
  userId!: string;
}
