import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CheckInviteByCodeDto {
  @ApiProperty()
  @IsNotEmpty()
  inviteCode!: number;
}
