import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
  ValidationPipe,
} from '@nestjs/common';
import { ApiCreatedResponse } from '@nestjs/swagger';
import { Flight } from '@entities/flight.entity';
import { Ticket } from '@entities/ticket.entity';

import {
  PresentationService,
  TCheckInviteResult,
} from './presentation.service';
import { ChangeStatusTicketToInMeetupDto } from './dto/change-status-ticket-to-in-meetup.dto';
import { CheckInviteByCodeDto } from './dto/check-invite-by-code.dto';
import { GenerateInviteDto } from './dto/generate-invite.dto';
import { QueryTakeSkipDto } from 'dto/query-take-skip.dto';
import { UserIdDto } from 'services/users.service/dto/user-id.dto';

@Controller('presentation')
export class PresentationController {
  constructor(private readonly presentation: PresentationService) {}

  @ApiCreatedResponse({ type: Ticket })
  @Put('/changeStatusTicketToInMeetup/:id')
  async changeStatusTicket(
    @Param('id') param: ChangeStatusTicketToInMeetupDto,
  ) {
    return await this.presentation.changeStatusTicketToInMeetup(param.id);
  }

  @ApiCreatedResponse({ type: Ticket })
  @Post('/generateInvite')
  async generateInvite(@Body(new ValidationPipe()) body: GenerateInviteDto) {
    return await this.presentation.generateInvite(body.userId);
  }

  @ApiCreatedResponse({ type: TCheckInviteResult })
  @Post('/checkInviteByCode')
  async checkInvite(@Body(new ValidationPipe()) body: CheckInviteByCodeDto) {
    return await this.presentation.checkInviteByCode(body.inviteCode);
  }

  @ApiCreatedResponse({ type: Ticket })
  @Get('/boarding')
  async boarding() {
    return await this.presentation.boarding();
  }

  @ApiCreatedResponse({ type: Flight })
  @Get('/map/:userId')
  async map(
    @Param(new ValidationPipe()) param: UserIdDto,
    @Query(new ValidationPipe()) query: QueryTakeSkipDto,
  ) {
    //const { take, skip, userId } = query;
    return await this.presentation.map({
      userId: param.userId,
      take: query.take!,
      skip: query.skip!,
    });
  }
}
