import { configService } from '../config.service';
import Redis from 'ioredis';
export class RedisService extends Redis {
  private constructor() {
    super({ host: configService.getRedis() });
  }

  private static instance: RedisService | null = null;
  public static async getInstance(): Promise<RedisService> {
    if (RedisService.instance == null) {
      RedisService.instance = new RedisService();
    }
    return RedisService.instance;
  }
}
