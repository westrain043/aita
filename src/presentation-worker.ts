import { Flight } from '@entities/flight.entity';
import 'reflect-metadata';
import { ConnectionOptions, createConnection, EntityManager } from 'typeorm';
import { configService } from './config.service';
import { User } from './entities/user.entity';
import { sleep } from './helpers';
import { RedisService } from './services/redis.service';

console.log('START');

const updateTime = 10000;

let take = 5,
  skip = 0,
  nextUser = 0;

const options = {
  ...configService.getTypeOrmConfig(),
  logging: false,
} as ConnectionOptions;

const nextSlide = async (manager: EntityManager) => {
  const [user] = await manager
    .getRepository(User)
    .find({ order: { createdAt: 'ASC' }, skip: nextUser });

  if (nextUser === 0 && !user) throw new Error('User not found');

  if (!user) nextUser = 0;

  const flights = await manager.getRepository(Flight).find({ take, skip });

  const flightsOr = await manager.getRepository(Flight).find({
    take,
    skip: skip + take,
  });

  (await RedisService.getInstance()).publish(
    `presentation`,
    JSON.stringify({ take, skip, userId: user.id }),
  );

  if (take !== flights.length || flightsOr.length === 0) skip = 0;
  else skip = take + skip;

  nextUser++;

  console.log('nextUser', nextUser, user.firstName);

  console.log('nextSlide');
};

createConnection(options).then(async (connection) => {
  while (true) {
    await sleep(updateTime);
    await nextSlide(connection.manager);
  }
});
