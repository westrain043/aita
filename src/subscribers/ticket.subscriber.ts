import { Injectable } from '@nestjs/common';
import { Ticket } from '@entities/ticket.entity';
import { EventStore } from '../eventstore';
import { ETicketStatus } from '../types/enums';
import {
  EntitySubscriberInterface,
  EventSubscriber,
  UpdateEvent,
} from 'typeorm';

@Injectable()
@EventSubscriber()
export class TicketSubscriber implements EntitySubscriberInterface<Ticket> {
  listenTo() {
    return Ticket;
  }

  afterUpdate(event: UpdateEvent<Ticket>) {
    const eventSource = EventStore.getInstance();

    if (event.entity.status == ETicketStatus.IN_MEETUP) {
      eventSource.publish('BOARDING', true);
    }
  }
}
