import { random } from 'lodash';
import { Ticket } from './entities/ticket.entity';

export function getDistanceInKm(
  lat1: number,
  lon1: number,
  lat2: number,
  lon2: number,
) {
  const R = 6371; // Radius of the earth in km
  const dLat = deg2rad(lat2 - lat1); // deg2rad below
  const dLon = deg2rad(lon2 - lon1);
  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) *
      Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  const d = R * c; // Distance in km
  return Math.round(d);
}

function deg2rad(deg: number) {
  return deg * (Math.PI / 180);
}

export const sleep = async (time: number) => {
  return new Promise((res) => {
    setTimeout(() => {
      res(true);
    }, time);
  });
};

export const generateCode = (tickets: Ticket[], code: number): number => {
  const genCode = random(40000, 99999);
  if (tickets.find((found) => found.inviteCode === code))
    return generateCode(tickets, genCode);
  return code;
};

export const data = [
  {
    city: 'Moscow',
    country: 'Russian',
    airport: 'Domodedovo',
    coordinates: '55.411143087823575, 37.90246728876811',
  },
  {
    city: 'Beauvais',
    country: 'Paris',
    airport: 'Beauvais Thiers Airport',
    coordinates: '49.45440489073941, 2.1114681814626284',
  },
  {
    city: 'Tel Aviv',
    country: 'Israel',
    airport: 'Ben Gurion Airport',
    coordinates: '32.12109136093753, 34.89750921215908',
  },
  {
    city: 'Western Australia',
    country: 'Australia',
    airport: 'Argyle',
    coordinates: '-16.395672644764222, 128.4115538855416',
  },
  {
    city: 'Stuttgart',
    country: 'Germany',
    airport: 'Stuttgart',
    coordinates: '48.785534023002, 9.235731498068178',
  },
  {
    city: 'New York',
    country: 'USA',
    airport: 'John F. Kennedy International Airport',
    coordinates: '40.64271238435269, -73.77774530146816',
  },
  {
    city: 'Los Angeles',
    country: 'USA',
    airport: 'Los Angeles International Airport',
    coordinates: '33.941366564599775, -118.40837534942614',
  },
  {
    city: 'Toronto',
    country: 'Canada',
    airport: 'Billy Bishop Airport',
    coordinates: '43.628551750465, -79.3959362400603',
  },
  {
    city: 'Tokyo',
    country: 'Japan',
    airport: 'Tokyo International Airport',
    coordinates: '35.54945426445472, 139.77983860134975',
  },
  {
    city: 'Texas',
    country: 'USA',
    airport: 'George Bush Airport',
    coordinates: '29.987388786047692, -95.3377539642444',
  },
];
